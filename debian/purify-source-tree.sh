#!/bin/sh

echo $(pwd)

# Remove all unwanted files/dirs.
echo "Now removing the whole contrib directory"
find . -name "contrib" -type d -exec rm -rfv '{}' \; >  /dev/null 2>&1

echo "Now removing the whole .svn directory"
find . -name ".svn" -type d -exec rm -rfv '{}' \; >  /dev/null 2>&1

echo "Now removing all the .exe files"
find . -name "*.exe" -type f -exec rm -rvf '{}' \;

echo "Now removing all the .pyc files"
find . -name "*.pyc" -type f -exec rm -rvf '{}' \;

echo "Now removing all the .git* files"
find . -name "\.git*" -type f -exec rm -rvf '{}' \;


# Remove all the exec bits from files that $(file) considers not
# binary.
for item in $(find -type f -executable)
do file ${item} | grep -i "elf "
    if [ "$?" != "0" ]
    then 
        echo "file ${item} not binary. Running chmod a-x on it."
        chmod a-x ${item}
    fi
done

